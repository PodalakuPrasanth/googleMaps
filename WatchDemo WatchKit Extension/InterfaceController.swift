//
//  InterfaceController.swift
//  WatchDemo WatchKit Extension
//
//  Created by Prasanth on 23/04/17.
//  Copyright © 2017 SreRama. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity


class InterfaceController: WKInterfaceController, WCSessionDelegate {


    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
           // Configure interface objects here.
    }
    
    
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
