//
//  ViewController.swift
//  WatchDemo
//
//  Created by Prasanth on 23/04/17.
//  Copyright © 2017 SreRama. All rights reserved.
//

import UIKit
import WatchConnectivity

class ViewController: UIViewController {

    
    let seesionW = WCSession.default()
    
    @IBOutlet weak var getlabel: UILabel!
    @IBOutlet weak var sendTxtField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(receivedMessage), name: NSNotification.Name(rawValue: "receiveWatchMessage"), object: nil)
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func sendButtonTapped(_ sender: Any) {
        
    
        if self.seesionW.isPaired == true && self.seesionW.isWatchAppInstalled {
            self.seesionW.sendMessage(["msg":self.sendTxtField.text!], replyHandler: nil, errorHandler: nil)
        }
    }

    func receivedMessage(info:NSNotification) {
        let message = info.userInfo!
        DispatchQueue.main.async{
            print(message[""] as! String)
        }
    }
    
    

}

